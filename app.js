var express = require('express')
var port = process.env.PORT || 3000
var app = express()

var server = app.listen(port, function(){
  console.log('listening on *:' + port)
})

var io = require('socket.io')(server)
// Use the gravatar module, to turn email addresses into avatar images:
var gravatar = require('gravatar');

app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/public'));

require('./soketMethod')(io, gravatar);
require('./routes')(app, io);
console.log('Your application is running on http://localhost:' + port);