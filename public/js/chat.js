// This file is executed in the browser, when people visit /chat/<random id>
$(function(){
	// getting the id of the room from the url
	//var id = Number(window.location.pathname.match(/\/chat\/(\d+)$/)[1]);
	var pathArr = window.location.pathname.split('/');
	var id = pathArr[2];
	// connect to the socket
	var socket = io();
	// variables which hold the data for each person
	var name = "", email = "",img = "",friend = "";
	// cache some jQuery objects
	var section = $(".section"),
	footer = $("footer"),
	chatScreen = $(".chatscreen"),
	noMessages = $(".nomessages"),
	tooManyPeople = $(".toomanypeople"),
	chatNickname = $(".nickname-chat"),
	chatForm = $("#chatform"),
	textarea = $("#message"),
	messageTimeSent = $(".timesent"),
	chats = $(".chats"),
	noMessagesImage = $("#noMessagesImage");

	// on connection to server get the id of person's room
	socket.on('connect', function(){
		socket.emit('load', id);
	});

	// save the gravatar url
	socket.on('img', function(data){
		img = data;
	});

	// receive the names and avatars of all people in the chat room
	socket.on('peopleinchat', function(data){
		if(data.number === 0){
			showMessage("connected");
			name = data.locals[2];			
			socket.emit('login', {user: data.locals[2], avatar: 'bansi@gmail.com', id: id, othername: data.locals[1]});
		}
		else if(data.number === 1) {
			showMessage("personinchat",data);
			name = data.locals[1];			
			socket.emit('login', {user: data.locals[1], avatar: 'bansi@gmail.com', id: id});
		}
		else {
			showMessage("tooManyPeople");
		}
	});

	// Other useful 

	socket.on('startChat', function(data){
		if(data.boolean && data.id == id) {
		//	chats.empty();
			if(data.roomLength == 0) {
				showMessage("youStartedChatWithNoMessages",data);
			}
			else {
				showMessage("heStartedChatWithNoMessages",data);
			}
			chatNickname.text(friend);
		}
	});

	socket.on('leave',function(data){
		if(data.boolean && id==data.room){
		//	chats.empty();
		}
	});

	socket.on('tooMany', function(data){
		if(data.boolean && name.length === 0) {
			showMessage('tooManyPeople');
		}
	});

	socket.on('receive', function(data){
		showMessage('chatStarted');
		if(data.msg.trim().length) {
			createChatMessage(data.msg, data.user, data.img, moment());
			scrollToBottom();
		}
	});

	textarea.keypress(function(e){
		// Submit the form on enter
		if(e.which == 13) {
			e.preventDefault();
			chatForm.trigger('submit');
		}
	});

	chatForm.on('submit', function(e){
		e.preventDefault();
		// Create a new chat message and display it directly
		showMessage("chatStarted");
		if(textarea.val().trim().length) {
			createChatMessage(textarea.val(), name, img, moment());
			scrollToBottom();
			// Send the message to the other person in the chat
			socket.emit('msg', {msg: textarea.val(), user: name, img: img});
		}		
		textarea.val(""); // Empty the textarea
	});

	// Update the relative time stamps on the chat messages every minute
	setInterval(function(){
		messageTimeSent.each(function(){
			var each = moment($(this).data('time'));
			$(this).text(each.fromNow());
		});
	},60000);

	// Function that creates a new chat message
	function createChatMessage(msg,user,imgg,now){
		var who = '';
		if(user===name) {	who = 'me';		}
		else {	who = 'you';	}

		var li = $(
			'<li class=' + who + '>'+
				'<div class="image">' +
					'<img src=' + imgg + ' />' +
					'<b></b>' +
					'<i class="timesent" data-time=' + now + '></i> ' +
				'</div>' +
				'<p></p>' +
			'</li>');

		// use the 'text' method to escape malicious user input
		li.find('p').text(msg);
		li.find('b').text(user);

		chats.append(li);

		messageTimeSent = $(".timesent");
		messageTimeSent.last().text(now.fromNow());
	}

	function scrollToBottom(){
		$("html, body").animate({ scrollTop: $(document).height()-$(window).height() },1000);
	}

	function showMessage(status,data){

		if(status === "connected"){
			section.children().css('display', 'none');
		}

		else if(status === "personinchat"){
			chatNickname.text(data.user);
		}

		else if(status === "youStartedChatWithNoMessages") {
			noMessages.fadeIn(1200);
			footer.fadeIn(1200);
			friend = data.users[1];
			noMessagesImage.attr("src",data.avatars[1]);
		}

		else if(status === "heStartedChatWithNoMessages") {
			noMessages.fadeIn(1200);
			footer.fadeIn(1200);
			friend = data.users[0];
			noMessagesImage.attr("src",data.avatars[0]);
		}

		else if(status === "chatStarted"){				
			section.children().css('display','none');
			chatScreen.css('display','block');
		}

		else if(status === "tooManyPeople") {
			section.children().css('display', 'none');
			tooManyPeople.fadeIn(1200);
		}
	}

});
