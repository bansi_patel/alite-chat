module.exports = function(app,io){
	app.get('/', function(req, res) {
		res.render('home');
	});

	app.get('/create/:proId/:sellerId/:clientId', function(req,res){
		//var id = Math.round((Math.random() * 1000000));
		var data = req.params
		res.redirect('/chat/'+data.proId+'_'+data.sellerId+'_'+data.clientId);
	});

	app.get('/chat/:id', function(req,res) {
		res.render('chat'); // Render the chant.html view
	});
};